#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include "string.h"

using namespace std;

void deleteWords(char*, char**, int);
void getDeleteTests(char*);
void getUserMode(char*);
void getDeleteLine(char*, char**, int);

int main()
{
	const int size = 256;
	char source[size] = "Lorem lorem   Ipsum, is simply  79 dummy text of the"
		" printing - and typesetting : % simply. Lorem Ipsum has been"
		" the industry's , simply dummy text ever since the 1500s, simply";

	while (true) {
		int n;
		cout << "Press 1 to enable test mode" << endl;
		cout << "Press 2 to delete your words" << endl;
		cout << "Press 3 to exit" << endl;
		cin >> n;
		system("cls");

		switch (n)
		{
		case 1:
			getDeleteTests(source);
			return 0;
			system("pause");
			break;
		case 2:
			getUserMode(source);
			return 0;
			break;
		case 3: 
			return 0;
		default:
			break;
		}
	}

	return 0;
}

void deleteWords(char* source, char** words, int count)
{
	for (int i = 0; i < count; i++) {
		char* word = words[i];
		int length = strlen(word);
		char* pword = strstr(source, word);

		while (pword) {
			char* p = pword + length;

			strcpy(pword, p);
			pword = strstr(source, word);
		}
	}
}

void getDeleteTests(char* source)
{
	const int size = 26;
	int count = 0;

	ifstream streamOut;
	streamOut.open("tests.txt");

	if (!streamOut.is_open()) {
		cout << "Cannot open this file";
		system("pause");
	}

	char** words = new char* [size];

	while (!streamOut.eof()) {
		char* word = new char[size];
		streamOut.getline(word, size);
		words[count] = word;
		count++;
	}

	getDeleteLine(source, words, count);
	streamOut.close();

}

void getUserMode(char* source)
{
	int count = 0, size = 26;
	cout << "Enter count of words: ";
	cin >> count;
	system("cls");

	char** words = new char* [size];

	for (int i = 0; i < count; i++) {
		char* word = new char[size];
		while (true) {
			cout << "Enter your word: " << endl;
			cin.getline(word, size);
			system("cls");

			if (word[0] != '\0') {
				break;
			}
		}
		words[i] = word;
	}

	getDeleteLine(source, words, count);

}

void getDeleteLine(char* source, char** words, int count)
{
	cout << "This is an original line: " << endl << source << endl << endl;
	cout << "This is line without ";

	for (int i = 0; i < count; i++)
	{
		if (i == count - 1) {
			cout << words[i] << ": ";
		}
		else {
			cout << words[i] << " ";
		}
	}

	deleteWords(source, words, count);
	cout << endl << source;
}
